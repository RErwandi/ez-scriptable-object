﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace EazyScriptableObject
{
    [GlobalConfig("Assets/Eazy ScriptableObject/Resources/")]
    public class EazySOSettings : GlobalConfig<EazySOSettings>
    {
        [FolderPath]
        public string scriptableObjectDatabase;

        [FolderPath]
        public string sceneInspectorDatabase;

        [FolderPath]
        public string codeGenerationDirectory;
        public bool codeGenerationAllowOverwrite = true;
        public bool gameEventLog = true;
        
        public List<BaseVariable> persistentVariables = new List<BaseVariable>();
        
        public const string persistentKey = "persistentVariables";

        [Button(ButtonSizes.Large)]
        public void SavePersistentVariables()
        {
            for (int i = 0; i < persistentVariables.Count; i++)
            {
                var json = JsonUtility.ToJson(persistentVariables[i]);
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create($"{Application.persistentDataPath}/{persistentKey}_{i}.pso");
                bf.Serialize(file, json);
                file.Close();
            }
            
            Debug.Log("Persistent Variables Saved on " + Application.persistentDataPath);
        }

        [Button(ButtonSizes.Large)]
        public void LoadPersistentVariables()
        {
            var variables = persistentVariables;
            for (int i = 0; i < variables.Count; i++) 
            {
                if (File.Exists($"{Application.persistentDataPath}/{persistentKey}_{i}.pso"))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = File.Open($"{Application.persistentDataPath}/{persistentKey}_{i}.pso", FileMode.Open);
                    JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file),variables[i]);
                    file.Close();
                }
            }
           
            Debug.Log("Persistent Variables Loaded");
        }

//        private void OnEnable()
//        {
//            var persistentSO = FindObjectOfType(typeof(PersistentScriptableObject));
//            if (persistentSO == null)
//            {
//                GameObject go = new GameObject();
//                go.name = "Persistent Variables Manager";
//                go.AddComponent(typeof(PersistentScriptableObject));
//            }
//        }
    }

//    public class PersistentScriptableObject : PersistentSingleton<PersistentScriptableObject>
//    {
//        private void OnEnable()
//        {
//            LoadPersistentVariables();
//        }
//
//        private void OnDisable()
//        {
//            SavePersistenceVariables();
//        }
//
//        private void OnApplicationPause(bool pauseStatus)
//        {
//            //SavePersistenceVariables();
//        }
//
//        private void OnApplicationFocus(bool hasFocus)
//        {
//            //SavePersistenceVariables();
//        }
//
//        private void OnApplicationQuit()
//        {
//            //SavePersistenceVariables();
//        }
//
//        public void SavePersistenceVariables()
//        {
//            var variables = EazySOSettings.Instance.persistentVariables;
//            
//            /*var json = JsonConvert.SerializeObject(variables, Formatting.Indented, new JsonSerializerSettings
//            {
//                TypeNameHandling = TypeNameHandling.Objects,
//                ObjectCreationHandling = ObjectCreationHandling.Replace
//                
//            });*/
//            for (int i = 0; i < variables.Count; i++)
//            {
//                var json = JsonUtility.ToJson(variables[i]);
//                BinaryFormatter bf = new BinaryFormatter();
//                FileStream file = File.Create($"{Application.persistentDataPath}/{EazySOSettings.persistentKey}_{i}.pso");
//                bf.Serialize(file, json);
//                file.Close();
//            }
//            
//            Debug.Log("Persistent Variables Saved on " + Application.persistentDataPath);
//        }
//
//        public void LoadPersistentVariables()
//        {
//            var variables = EazySOSettings.Instance.persistentVariables;
//            for (int i = 0; i < variables.Count; i++) 
//            {
//                if (File.Exists($"{Application.persistentDataPath}/{EazySOSettings.persistentKey}_{i}.pso"))
//                {
//                    BinaryFormatter bf = new BinaryFormatter();
//                    FileStream file = File.Open($"{Application.persistentDataPath}/{EazySOSettings.persistentKey}_{i}.pso", FileMode.Open);
//                    JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file),variables[i]);
//                    file.Close();
//                } else 
//                {
//                    //Do Nothing
//                }
//            }
//            /*EazySOSettings.Instance.persistentVariables = JsonConvert.DeserializeObject<List<BaseVariable>>(json, new JsonSerializerSettings
//            {
//                TypeNameHandling = TypeNameHandling.Objects,
//                ObjectCreationHandling = ObjectCreationHandling.Replace
//            });*/
//            Debug.Log("Persistent Variables Loaded");
//        }
//    }
}