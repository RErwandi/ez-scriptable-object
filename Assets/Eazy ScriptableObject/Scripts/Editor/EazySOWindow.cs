﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EazyScriptableObject
{
    public class EazySOWindow : OdinMenuEditorWindow
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Eazy ScriptableObject/Manager")]
        private static void OpenWindow()
        {
            var window = GetWindow<EazySOWindow>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 500);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree(true);
            tree.DefaultMenuStyle.IconSize = 28.00f;
            tree.Config.DrawSearchToolbar = true;
            
            // Add all Scene Inspector in database folder
            tree.AddAllAssetsAtPath("Scene Inspector", EazySOSettings.Instance.sceneInspectorDatabase, typeof(SerializedScriptableObject), true).SortMenuItemsByName();
            
            // Add all ScriptableObjects in database folder.
            tree.AddAllAssetsAtPath("ScriptableObjects", EazySOSettings.Instance.scriptableObjectDatabase, typeof(ScriptableObject), true).SortMenuItemsByName();
            
            tree.Add("Code Generator", CreateInstance<EazySOCodeGenerationWindow>());
            
            // Add Settings
            tree.Add("Settings", EazySOSettings.Instance);
            
            return tree;
        }
#endif
    }
}