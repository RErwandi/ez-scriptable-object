﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

namespace EazyScriptableObject
{
    public class EazySOCodeGenerationWindow : OdinEditorWindow
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Eazy ScriptableObject/Code Generator")]
        private static void OpenWindow()
        {
            GetWindow<EazySOCodeGenerationWindow>().Show();
        }

        public string typeName;
        public string nameSpace;
        [TableList] public List<Field> fields = new List<Field>();

        [Button(ButtonSizes.Large)]
        public void Generate()
        {
            EazySOCodeGenerator.Data data = new EazySOCodeGenerator.Data()
            {
                TypeName = typeName,
                NameSpace = nameSpace,
                FieldString = GenerateFieldString()
            };
            
            EazySOCodeGenerator.Generate(data);
            AssetDatabase.Refresh();
        }

        private string GenerateFieldString()
        {
            var fieldString = "";

            foreach (var field in fields)
            {
                fieldString += $"public {field.typeName} {field.fieldName};\n\t\t";
            }
            
            return fieldString;
        }

        [Serializable]
        public class Field
        {
            public string typeName;
            public string fieldName;
        }
#endif
    }
}