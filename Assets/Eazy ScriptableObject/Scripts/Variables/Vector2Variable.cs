using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector2Variable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Vector2",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class Vector2Variable : BaseVariable<Vector2>
    {
    } 
}