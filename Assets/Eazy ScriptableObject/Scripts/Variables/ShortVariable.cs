﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ShortVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "short",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class ShortVariable : BaseVariable<short>
    {
    } 
}