﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace EazyScriptableObject
{
    public abstract class BaseVariable : GameEventBase
    {
    }
    
    public abstract class BaseVariable<T> : BaseVariable
    {
        // Current value
        [BoxGroup("Debugging Options")]
        [SerializeField] private T value;
        
        public T Value
        {
            get => value;
            set
            {
                this.value = value;
                Raise();
            }
        }

        public override string ToString()
        {
            return Value == null ? "null" : Value.ToString();
        }
        
        public static implicit operator T(BaseVariable<T> variable)
        {
            return variable.Value;
        }
    } 
}