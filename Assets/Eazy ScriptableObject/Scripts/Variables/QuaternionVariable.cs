using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "QuaternionVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Quaternion",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class QuaternionVariable : BaseVariable<Quaternion>
    {
    } 
}