﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "DoubleVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "double",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class DoubleVariable : BaseVariable<double>
    {
    } 
}