﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "FloatVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "float",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class FloatVariable : BaseVariable<float>
    {
    } 
}