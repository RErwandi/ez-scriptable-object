using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "ColorVariable.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Color",
	    order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
	public class ColorVariable : BaseVariable<Color>
	{
	}
}