﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ByteVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "byte",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class ByteVariable : BaseVariable<byte>
    {
    } 
}