﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Short GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "short",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class ShortGameEventInspector : ScriptableObjectSceneInspector<short, ShortGameEvent, ShortUnityEvent>
    {
        
    }
}