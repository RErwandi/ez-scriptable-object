﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Double GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "double",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class DoubleGameEventInspector : ScriptableObjectSceneInspector<double, DoubleGameEvent, DoubleUnityEvent>
    {
        
    }
}