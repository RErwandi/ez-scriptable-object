﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "UInt GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "uint",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class UIntGameEventInspector : ScriptableObjectSceneInspector<uint, UIntGameEvent, UIntUnityEvent>
    {
        
    }
}