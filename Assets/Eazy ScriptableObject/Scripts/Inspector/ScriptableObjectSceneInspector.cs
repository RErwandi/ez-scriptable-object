﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace EazyScriptableObject
{
    public abstract class ScriptableObjectSceneInspector<TType, TEvent, TResponse> : SerializedScriptableObject
    where TEvent : GameEventBase<TType>
    where TResponse : UnityEvent<TType>
    {
#if UNITY_EDITOR

        #region Singleton

        static ScriptableObjectSceneInspector<TType, TEvent, TResponse> instance = null;
            public static ScriptableObjectSceneInspector<TType, TEvent, TResponse> Instance
            {
                get
                {
                    if (!instance)
                        instance = Resources.FindObjectsOfTypeAll<ScriptableObjectSceneInspector<TType, TEvent, TResponse>>().FirstOrDefault();
                    return instance;
                }
            }

        #endregion
        // Dictionary of all game objects that are using the game event listener
        [DictionaryDrawerSettings(KeyLabel = "Events", ValueLabel = "Used On")]
        [SerializeField] [ReadOnly] private Dictionary<TEvent, List<GameObject>> gameEventDictionary =
            new Dictionary<TEvent, List<GameObject>>();

        // All the game object with game event listener
        [HorizontalGroup("Split", 0.5f, LabelWidth = 20)]
        [BoxGroup("Split/Assigned")]
        [LabelText("Game Event Listeners")]
        [SerializeField] [ReadOnly] private BaseGameEventListener<TType, TEvent, TResponse>[] eventObjects;
        
        [BoxGroup("Split/Unassigned")]
        [LabelText("Game Event Listeners")]
        // List of all event listeners that aren't assigned
        [SerializeField] [ReadOnly] private List<BaseGameEventListener<TType, TEvent, TResponse>> unassignedEventListeners = new List<BaseGameEventListener<TType, TEvent, TResponse>>();

        private void OnEnable()
        {
            SceneManager.activeSceneChanged += SceneUpdate;
            EditorApplication.hierarchyChanged += HierarchyChanged;
            
            GetAllScriptableEventsInScene();
        }

        private void OnDisable()
        {
            SceneManager.activeSceneChanged -= SceneUpdate;
            EditorApplication.hierarchyChanged -= HierarchyChanged;
        }

        private void HierarchyChanged()
        {
            GetAllScriptableEventsInScene();
        }

        private void SceneUpdate(Scene current, Scene next)
        {
            GetAllScriptableEventsInScene();
        }
        
        private void GetAllScriptableEventsInScene()
        {
            ClearCollections();
            
            // Get All the game object with game event listener
            eventObjects = FindObjectsOfType<BaseGameEventListener<TType, TEvent, TResponse>>();

            foreach (var listener in eventObjects)
            {
                if (listener.gameEvent != null)
                {
                    // Check to see if the key does not already exist in the dictionary
                    if (!gameEventDictionary.ContainsKey(listener.gameEvent))
                    {
                        List<GameObject> eventObjectList = new List<GameObject>();
                        eventObjectList.Add(listener.gameObject);
                        gameEventDictionary.Add(listener.gameEvent, eventObjectList); // Add the event and the gameobject list
                    }
                    else
                    {
                        List<GameObject> copyList = gameEventDictionary[listener.gameEvent]; // Get a copy of the list associated with listener event
                        copyList.Add(listener.gameObject);

                        gameEventDictionary[listener.gameEvent] = copyList;
                    }
                }
                else // Add to the unassignedObject list
                {
                    unassignedEventListeners.Add(listener);
                }
            }
        }

        void ClearCollections()
        {
            gameEventDictionary.Clear();
            unassignedEventListeners.Clear();
        }
#endif
    }
    
    public abstract class ScriptableObjectSceneInspector<TEvent, TResponse> : SerializedScriptableObject
    where TEvent : GameEventBase
    where TResponse : UnityEvent
    {
#if UNITY_EDITOR

        #region Singleton

        static ScriptableObjectSceneInspector<TEvent, TResponse> instance = null;
            public static ScriptableObjectSceneInspector<TEvent, TResponse> Instance
            {
                get
                {
                    if (!instance)
                        instance = Resources.FindObjectsOfTypeAll<ScriptableObjectSceneInspector<TEvent, TResponse>>().FirstOrDefault();
                    return instance;
                }
            }

        #endregion
        // Dictionary of all game objects that are using the game event listener
        [DictionaryDrawerSettings(KeyLabel = "Events", ValueLabel = "Used On")]
        [SerializeField] [ReadOnly] private Dictionary<TEvent, List<GameObject>> gameEventDictionary =
            new Dictionary<TEvent, List<GameObject>>();

        // All the game object with game event listener
        [HorizontalGroup("Split", 0.5f, LabelWidth = 20)]
        [BoxGroup("Split/Assigned")]
        [LabelText("Game Event Listeners")]
        [SerializeField] [ReadOnly] private BaseGameEventListener<TEvent, TResponse>[] eventObjects;
        
        [BoxGroup("Split/Unassigned")]
        [LabelText("Game Event Listeners")]
        // List of all event listeners that aren't assigned
        [SerializeField] [ReadOnly] private List<BaseGameEventListener<TEvent, TResponse>> unassignedEventListeners = new List<BaseGameEventListener<TEvent, TResponse>>();

        private void OnEnable()
        {
            SceneManager.activeSceneChanged += SceneUpdate;
            EditorApplication.hierarchyChanged += HierarchyChanged;
            
            GetAllScriptableEventsInScene();
        }

        private void OnDisable()
        {
            SceneManager.activeSceneChanged -= SceneUpdate;
            EditorApplication.hierarchyChanged -= HierarchyChanged;
        }

        private void HierarchyChanged()
        {
            GetAllScriptableEventsInScene();
        }

        private void SceneUpdate(Scene current, Scene next)
        {
            GetAllScriptableEventsInScene();
        }
        
        private void GetAllScriptableEventsInScene()
        {
            ClearCollections();
            
            // Get All the game object with game event listener
            eventObjects = FindObjectsOfType<BaseGameEventListener<TEvent, TResponse>>();

            foreach (var listener in eventObjects)
            {
                if (listener.gameEvent != null)
                {
                    // Check to see if the key does not already exist in the dictionary
                    if (!gameEventDictionary.ContainsKey(listener.gameEvent))
                    {
                        List<GameObject> eventObjectList = new List<GameObject>();
                        eventObjectList.Add(listener.gameObject);
                        gameEventDictionary.Add(listener.gameEvent, eventObjectList); // Add the event and the gameobject list
                    }
                    else
                    {
                        List<GameObject> copyList = gameEventDictionary[listener.gameEvent]; // Get a copy of the list associated with listener event
                        copyList.Add(listener.gameObject);

                        gameEventDictionary[listener.gameEvent] = copyList;
                    }
                }
                else // Add to the unassignedObject list
                {
                    unassignedEventListeners.Add(listener);
                }
            }
        }

        void ClearCollections()
        {
            gameEventDictionary.Clear();
            unassignedEventListeners.Clear();
        }
#endif
    }
}