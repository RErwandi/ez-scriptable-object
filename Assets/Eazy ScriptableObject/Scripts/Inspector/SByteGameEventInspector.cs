﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "SByte GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "sbyte",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class SByteGameEventInspector : ScriptableObjectSceneInspector<sbyte, SByteGameEvent, SByteUnityEvent>
    {
        
    }
}