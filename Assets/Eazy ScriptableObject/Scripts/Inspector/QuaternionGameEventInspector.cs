﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Quaternion GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "quaternion",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class QuaternionGameEventInspector : ScriptableObjectSceneInspector<Quaternion, QuaternionGameEvent, QuaternionUnityEvent>
    {
        
    }
}