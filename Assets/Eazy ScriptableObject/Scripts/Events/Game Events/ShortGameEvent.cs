﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "ShortGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "short",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class ShortGameEvent : GameEventBase<short>
    {
    } 
}