﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "CharGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "char",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class CharGameEvent : GameEventBase<char>
    {
    } 
}