﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "StringGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "string",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class StringGameEvent : GameEventBase<string>
    {
    } 
}