﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "LongGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "long",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class LongGameEvent : GameEventBase<long>
    {
    } 
}