﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "UnsignedIntGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "uint",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class UIntGameEvent : GameEventBase<uint>
    {
    } 
}