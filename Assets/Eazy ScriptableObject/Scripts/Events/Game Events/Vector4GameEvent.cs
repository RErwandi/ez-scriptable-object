using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "Vector4GameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU +  "Structs/Vector4",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class Vector4GameEvent : GameEventBase<Vector4>
    {
    } 
}