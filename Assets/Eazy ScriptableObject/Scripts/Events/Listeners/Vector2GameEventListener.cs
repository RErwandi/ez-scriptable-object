using UnityEngine;

namespace EazyScriptableObject
{
    [AddComponentMenu(EazySOUtility.EVENT_LISTENER_SUBMENU + "Vector2 Event Listener")]
    public sealed class Vector2GameEventListener : BaseGameEventListener<Vector2, Vector2GameEvent, Vector2UnityEvent>
    {
    }
}