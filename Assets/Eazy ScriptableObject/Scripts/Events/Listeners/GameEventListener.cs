﻿using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [AddComponentMenu(EazySOUtility.EVENT_LISTENER_SUBMENU + "Game Event Listener")]
    [ExecuteInEditMode]
    public sealed class GameEventListener : BaseGameEventListener<GameEventBase, UnityEvent>
    {
    }
}