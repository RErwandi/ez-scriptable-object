﻿using UnityEngine;

namespace EazyScriptableObject
{
    [AddComponentMenu(EazySOUtility.EVENT_LISTENER_SUBMENU + "short Event Listener")]
    public sealed class ShortGameEventListener : BaseGameEventListener<short, ShortGameEvent, ShortUnityEvent>
    {
    }
}