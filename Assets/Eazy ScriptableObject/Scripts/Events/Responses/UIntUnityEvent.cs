﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class UIntUnityEvent : UnityEvent<uint>
    {
    } 
}