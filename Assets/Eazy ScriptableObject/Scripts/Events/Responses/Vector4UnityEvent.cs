using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class Vector4UnityEvent : UnityEvent<Vector4>
    {
    } 
}