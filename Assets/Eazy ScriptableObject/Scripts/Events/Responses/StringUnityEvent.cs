﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class StringUnityEvent : UnityEvent<string>
    {
    } 
}