using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class Vector3UnityEvent : UnityEvent<Vector3>
    {
    } 
}