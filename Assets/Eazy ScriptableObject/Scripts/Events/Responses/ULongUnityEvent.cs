﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class ULongUnityEvent : UnityEvent<ulong>
    {
    } 
}