using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "StringCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "string",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class StringCollection : Collection<string>
    {
    } 
}