using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "CharCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "char",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class CharCollection : Collection<char>
    {
    } 
}