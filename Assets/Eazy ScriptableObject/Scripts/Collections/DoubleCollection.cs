using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "DoubleCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "double",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class DoubleCollection : Collection<double>
    {
    } 
}