using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "UShortCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "ushort",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class UShortCollection : Collection<ushort>
    {
    } 
}