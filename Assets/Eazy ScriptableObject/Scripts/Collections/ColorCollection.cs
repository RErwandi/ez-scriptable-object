using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "ColorCollection.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Color",
	    order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
	public class ColorCollection : Collection<Color>
	{
	}
}