using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "FloatCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "float",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class FloatCollection : Collection<float>
    {
    } 
}