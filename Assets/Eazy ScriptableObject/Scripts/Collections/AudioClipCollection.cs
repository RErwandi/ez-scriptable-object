using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "AudioClipCollection.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "AudioClip",
	    order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
	public class AudioClipCollection : Collection<AudioClip>
	{
	}
}