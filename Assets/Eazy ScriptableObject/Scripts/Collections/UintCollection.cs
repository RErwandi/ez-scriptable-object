using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "UIntCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "uint",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class UIntCollection : Collection<uint>
    {
    } 
}