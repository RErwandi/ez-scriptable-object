﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace EazyScriptableObject
{
    public class ImageFillFloatSetter : MonoBehaviour, IGameEventListener
    {
        [SerializeField] private FloatVariable currentHp = default;
        [SerializeField] private FloatVariable maxHp = default;
        [SerializeField] private Image image = default;

        private void OnEnable()
        {
            currentHp.AddListener(this);
            maxHp.AddListener(this);
        }

        private void OnDisable()
        {
            currentHp.RemoveListener(this);
            maxHp.AddListener(this);
        }

        private void Start()
        {
            Repaint();
        }

        public void OnEventRaised()
        {
            Repaint();
        }

        private void Repaint()
        {
            image.fillAmount = Mathf.Clamp01(currentHp.Value / maxHp.Value);
        }
    }
}
